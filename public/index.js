let inputObj = {};

function handleTextRadioClick() {
    let textChoice = document.querySelector('input[name="drone"]:checked').value;
    console.log(textChoice);
    toggleElement('character-calculation-wrapper');
    toggleElement('manual-character-input');
}

function toggleElement(id) {
    let x = document.getElementById(id);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function handleTextAreaChange() {
    let value = document.getElementById("manual-text-entry").value;
    value = value.trim();
    let characters = value.split(' ').join('').length;
    setCharacterLength(characters);
    document.getElementById("calculatedTextCharacters").setAttribute('value', characters)
}

function handleCharacterInputChange() {
    let value = document.getElementById("inputTextCharacters").value;
    setCharacterLength(value);
}

function setCharacterLength(characters) {
    inputObj.characters = characters;
    console.log('Character length: ' + inputObj.characters);
}

function calculateReadingTime() {
    let x = document.getElementById('calculate-time-element');
    x.style.display = "block";
}
